require 'pp'

graph = {
  a: {
    neighbors: [{b: 4}, {h: 8}],
    key: Float::INFINITY,
    parents: nil
  },
  b: {
    neighbors: [{a: 4}, {c: 8}, {h: 11}],
    key: Float::INFINITY,
    parents: nil
  },
  c: {
    neighbors: [{b: 8}, {d: 7}, {f: 4}, {i: 2}],
    key: Float::INFINITY,
    parents: nil
  },
  d: {
    neighbors: [{c: 7}, {e: 9}, {f: 14}],
    key: Float::INFINITY,
    parents: nil
  },
  e: {
    neighbors: [{d: 9}, {f: 10}],
    key: Float::INFINITY,
    parents: nil
  },
  f: {
    neighbors: [{c: 4}, {d: 14}, {e: 10}, {g: 2}],
    key: Float::INFINITY,
    parents: nil
  },
  g: {
    neighbors: [{f: 2}, {h: 1}, {i: 6}],
    key: Float::INFINITY,
    parents: nil
  },
  h: {
    neighbors: [{a: 8}, {g: 1}, {i: 7}],
    key: Float::INFINITY,
    parents: nil
  },
  i: {
    neighbors: [{c: 2}, {g: 6}, {h: 7}],
    key: Float::INFINITY,
    parents: nil
  }
}

def union queue, u, v
  last_id = queue.last[:id] + 1
  set_u = []
  set_v = []
  id_1, id_2 = nil, nil
  queue.each do |a|
    if a[:set].include?(u)
      set_u = a[:set]
      id_1 = a
    end
    if a[:set].include?(v)
      set_v = a[:set]
      id_2 = a
    end
  end
  queue.delete(id_1)
  queue.delete(id_2)
  queue << { id: last_id, set: set_u + set_v }
end

def find_set queue, u
  queue.each do |a|
    if a[:set].include?(u)
      return a[:id]
    end
  end
end

def kruskal graph
  the_a = []

  queue = []

  i = 0
  graph.each do |k, value|
    queue << { id: i, set: [k] }
    i = i + 1
  end

  edges = []

  graph.each do |q, value|
    graph[q][:neighbors].each do |v|
      v_key = v.keys[0]
      v_value = v.values[0]
      unless edges.include?({ u: v_key, v: q, w: v_value })
        edges << { u: q, v: v_key, w: v_value }
      end
    end
  end

  edges.sort_by!{ |a| a[:w] }

  edges.each do |b|
    if find_set(queue, b[:u]) != find_set(queue, b[:v])
      pp "--- Before ---"
      pp queue
      the_a << b
      union(queue, b[:u], b[:v])
      pp "--- After ---"
      pp queue
    end
  end

  pp the_a
end

kruskal(graph)