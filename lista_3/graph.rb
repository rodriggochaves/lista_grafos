# Patrícia Deud Guimarães 13/0015989
# Rodrigo de Araujo Chaves 13/0132624

graph1 = {
  a: {
    neighbors: [:d, :e, :f]
  },
  b: {
    neighbors: [:d, :e, :f]
  },
  c: {
    neighbors: [:d, :e, :f]
  },
  d: {
    neighbors: [:a, :b, :c]
  },
  e: {
    neighbors: [:a, :b, :c]
  },
  f: {
    neighbors: [:a, :b, :c]
  }
}

graph2 = {
  a: {
    neighbors: [:f]
  },
  b: {
    neighbors: [:f, :g]
  },
  c: {
    neighbors: [:h, :i]
  },
  d: {
    neighbors: [:g]
  },
  e: {
    neighbors: [:f, :i]
  },
  f: {
    neighbors: [:a, :b, :e]
  },
  g: {
    neighbors: [:b, :d]
  },
  h: {
    neighbors: [:c]
  },
  i: {
    neighbors: [:c, :e]
  }
}

graph3 = {
  a: {
    neighbors: [:b, :c]
  },
  b: {
    neighbors: [:a, :d]
  },
  c: {
    neighbors: [:a, :d]
  },
  d: {
    neighbors: [:b, :c, :e]
  },
  e: {
    neighbors: [:d, :f, :g, :h]
  },
  f: {
    neighbors: [:e]
  },
  g: {
    neighbors: [:e]
  },
  h: {
    neighbors: [:e, :i]
  },
  i: {
    neighbors: [:h]
  }
}

graph4 = {
  a: {
    neighbors: [:c, :b]
  },
  b: {
    neighbors: [:a, :c]
  },
  c: {
    neighbors: [:a, :b]
  }
}

def graph_two_colors graph
  graph.each do |key, value|
    value[:color] = -1
  end

  graph.each do |key, value|
    if value[:color] == -1
      if ! dfs_color(graph, key, 0)
        return false
      end
    end
  end

  return true;
end


def dfs_color graph, v, color
  graph[v][:color] = color  
  graph[v][:neighbors].each do |w|
    if graph[w][:color] == -1
      if ! dfs_color(graph, w, 1 - color)
        return false;
      end
    else
      if graph[w][:color] == color
        return false
      end
    end
  end
  return true
end

teste1 = graph_two_colors graph1
p graph1
p teste1
# graph_two_colors graph2
# p graph2
# graph_two_colors graph3
# p graph3
teste = graph_two_colors graph4
p graph4
p teste