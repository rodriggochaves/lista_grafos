require 'JSON'
require 'pp'

graph = {
  a: {
    neighbors: [:b, :f]
  },
  b: {
    neighbors: [:a, :e]
  },
  c: {
    neighbors: [:d, :f, :g]
  },
  d: {
    neighbors: [:c, :g, :h]
  },
  e: {
    neighbors: [:b]
  },
  f: {
    neighbors: [:a, :c, :g]
  },
  g: {
    neighbors: [:c, :d, :f, :h]
  },
  h: {
    neighbors: [:d, :g]
  }
}

def breadth_first_search graph, root
  graph.each do |k, v|
    graph[k][:color] = :white
    graph[k][:distance] = -1
    graph[k][:father] = nil
  end

  queue = []
  graph[root][:color] = :gray
  graph[root][:distance] = 0
  queue << root
  while ! queue.empty?
    u = queue.pop
    graph[u][:neighbors].each do |v|
      if graph[v][:color] == :white
        graph[v][:color] = :gray
        graph[v][:distance] = graph[u][:distance] + 1
        graph[v][:father] = u
        queue.push v
      end
    end
    graph[u][:color] = :black
  end
end

breadth_first_search graph, :a

# pp graph
puts JSON.pretty_generate(graph).gsub(":", " =>")
