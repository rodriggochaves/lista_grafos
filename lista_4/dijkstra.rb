require 'pp'

graph = {
  s: {
    neighbors: [{u: 5}, {r: 10}]
  },
  r: {
    neighbors: [{w: 1}, {u: 2}]
  },
  u: {
    neighbors: [{r: 3}, {v: 2}, {w: 9}]
  },
  w: {
    neighbors: [{v: 4}, {t: 3}]
  },
  v: {
    neighbors: [{w: 6}, {t: 5}, {s: 7}]
  },
  t: {
    neighbors: []
  }
}

def extract_min queue, graph
  return_queue = []
  queue.each do |v|
    minor = return_queue.first || queue[0]
    if graph[v][:distance] < graph[minor][:distance]
      return_queue.unshift(v)
    else
      return_queue.push(v)
    end
  end
  return return_queue.shift
end

def key v
  return v.keys[0]
end

def value v
  return v.values[0]
end

def dijkstra graph, s
  queue = []

  graph.each do |k, v|
    graph[k][:distance] = Float::INFINITY
    graph[k][:father] = nil
    queue << k
  end

  graph[s][:distance] = 0

  while ! queue.empty?
    u = extract_min queue, graph
    queue.delete u
    graph[u][:neighbors].each do |v|
      if graph[key(v)][:distance] > graph[u][:distance] + value(v)
        graph[key(v)][:distance] = graph[u][:distance] + value(v)
        graph[key(v)][:father] = u
      end
    end
  end
end

dijkstra(graph, :s)

pp graph
