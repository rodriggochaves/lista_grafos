# Patrícia Guimarães 13/0015989
# Rodrigo Chaves 13/0132624 
# Carlos Tavares 13/0007293

require 'pp'

def depth_first_search(graph)
  graph.each do |key, value|
    value[:color] = 'white'
    value[:father] = nil
  end
  time = 0
  graph.each do |key, value|
    if graph[key][:color] == 'white'
      graph, time = visit(graph, key, time)
    end
  end
end

def visit graph, key, time 
  graph[key][:color] = 'gray'
  time = time + 1
  # p "Visitando node #{key} no tempo minimo de #{time}"
  graph[key][:minimum_distance] = time
  graph[key][:neighbors].each do |n|
    if graph[n][:color] == 'white'
      graph[n][:father] = key
      graph, time = visit(graph, n, time)
    end
  end
  graph[key][:color] = 'black'
  time = time + 1
  # p "Visitando node #{key} no tempo maximo #{time}"
  graph[key][:maximum_distance] = time
  return graph, time
end

def visit graph, key, time 
  graph[key][:color] = 'gray'
  time = time + 1
  graph[key][:minimum_distance] = time
  graph[key][:neighbors].each do |n|
    if graph[n][:color] == 'white'
      graph[n][:father] = key
      graph, time = visit(graph, n, time)
    end
  end
  graph[key][:color] = 'black'
  time = time + 1
  # p "Visitando node #{key} no tempo maximo #{time}"
  graph[key][:maximum_distance] = time
  return graph, time
end

def transposed graph
  edges = []
  graph.each do |key, value|
    value[:neighbors].each do |e|
      edges << [key, e]
    end
  end
  edges
end

def recreate_graph edges
  father = 1
  son = 0
  graph = {}
  edges.each do |edge|
    if graph[edge[father]]
      graph[edge[father]][:neighbors] << edge[son]
    else
      graph[edge[father]] = {}
      graph[edge[father]][:visited] = false
      graph[edge[father]][:neighbors] = [edge[son]]
    end
    # check son is in the graph already
    if not graph[edge[son]]
      graph[edge[son]] = {
        visited: false,
        neighbors: []
      }
    end
  end
  graph
end

def order graph
  graph = graph.sort_by{ |e| -e[1][:maximum_distance] }
  graph
end

def strongly_connected(graph, transposed_graph, list)
  graph.each do |key, value|
    transposed_graph[key][:maximum_distance] = graph[key][:maximum_distance]
    transposed_graph[key][:minimum_distance] = graph[key][:minimum_distance]
  end
  transposed_graph = order(transposed_graph)
  transposed_graph.each do |key, value|
    value[:color] = 'white'
    value[:father] = nil
  end
  hash_transposed_graph = {}
  transposed_graph.each do |value|
    hash_transposed_graph[value[0]] = value[1]
  end
  time = 0
  hash_transposed_graph.each do |key, value|
    if hash_transposed_graph[key][:color] == 'white'
      tree = []
      tree << key
      hash_transposed_graph, time, tree = visit2(hash_transposed_graph, key, time, tree)
      list << tree
    end
  end
end

def visit2 graph, key, time, tree
  graph[key][:color] = 'gray'
  time = time + 1
  graph[key][:minimum_distance] = time
  graph[key][:neighbors].each do |n|
    if graph[n][:color] == 'white'
      graph[n][:father] = key
      tree << n
      graph, time = visit2(graph, n, time, tree)
    end
  end
  graph[key][:color] = 'black'
  time = time + 1
  # p "Visitando node #{key} no tempo maximo #{time}"
  graph[key][:maximum_distance] = time
  return graph, time, tree
end

graph = {
  a: {
    neighbors: [:b]
  },
  b: {
    neighbors: [:c, :e, :f]
  },
  c: {
    neighbors: [:d, :g]
  },
  d: {
    neighbors: [:c, :h]
  },
  e: {
    neighbors: [:a, :f]
  },
  f: {
    neighbors: [:g]
  },
  g: {
    neighbors: [:f, :h]
  },
  h: {
    neighbors: [:h]
  }
}

list = []
depth_first_search(graph)
edges = transposed(graph)
transposed_graph = recreate_graph(edges)
strongly_connected(graph, transposed_graph, list)
pp list