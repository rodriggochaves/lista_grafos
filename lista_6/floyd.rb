require 'pp'

# le a entrada

n, m = gets.split(" ")
n = n.to_i
streets = Array.new(n){ Array.new(n, Float::INFINITY) }

m = m.to_i

m.times do
  a, b, w = gets.split(" ")
  a =  a.to_i - 1
  b = b.to_i - 1
  w = w.to_f
  if a == b
    streets[a][b] = 0.0
  else
    streets[a][b] = w
    streets[b][a] = w
  end
end

for i in 0..(n - 1)
  for j in 0..(n - 1)
    if i == j
      streets[i][j] = 0.0
    end
  end
end

# inicializacao

dist = streets.clone
parents = Array.new(n) { Array.new(n, nil) }

for i in 0..(n - 1)
  for j in 0..(n - 1)
    if i == j || dist[i][j] == Float::INFINITY
      parents[i][j] = nil
    elsif i != j && dist[i][j] < Float::INFINITY
      parents[i][j] = i + 1
    end
  end
end

# processamento

for k in 0..(n - 1)
  for i in 0..(n - 1)
    for j in 0..(n - 1)
      if dist[i][j] > dist[i][k] + dist[k][j]
        dist[i][j] = dist[i][k] + dist[k][j]
        parents[i][j] = parents[k][j]
      end
    end
  end
end

# pp dist
# pp parents

paths = []
d = 5

def find_path parents, paths, a, destiny
  # a: represents

  intermediates = ((0 + (a + 1))..(5)).to_a

  intermediates.each do |i|
    b = parents[a][i - 1]
    # is direct connection?
    if b == (a + 1)
      paths << [i]
    end
  end

  pp paths
end

find_path parents, paths, 0, 5