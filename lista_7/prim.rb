require 'pp'

graph = {
  a: {
    neighbors: [{b: 4}, {h: 8}],
    key: Float::INFINITY,
    parents: nil
  },
  b: {
    neighbors: [{a: 4}, {c: 8}, {h: 11}],
    key: Float::INFINITY,
    parents: nil
  },
  c: {
    neighbors: [{b: 8}, {d: 7}, {f: 4}, {i: 2}],
    key: Float::INFINITY,
    parents: nil
  },
  d: {
    neighbors: [{c: 7}, {e: 9}, {f: 14}],
    key: Float::INFINITY,
    parents: nil
  },
  e: {
    neighbors: [{d: 9}, {f: 10}],
    key: Float::INFINITY,
    parents: nil
  },
  f: {
    neighbors: [{c: 4}, {d: 14}, {e: 10}, {g: 2}],
    key: Float::INFINITY,
    parents: nil
  },
  g: {
    neighbors: [{f: 2}, {h: 1}, {i: 6}],
    key: Float::INFINITY,
    parents: nil
  },
  h: {
    neighbors: [{a: 8}, {g: 1}, {i: 7}],
    key: Float::INFINITY,
    parents: nil
  },
  i: {
    neighbors: [{c: 2}, {g: 6}, {h: 7}],
    key: Float::INFINITY,
    parents: nil
  }
}

def w graph, u, v
  graph[u][:neighbors].each do |n|
    n_key = n.keys[0]
    if n_key == v
      return n.values[0]
    end
  end
end

def returns_min queue, graph
  minor = {
    id: nil,
    value: Float::INFINITY
  }
  queue.each do |q|
    if graph[q][:key] < minor[:value]
      minor[:id] = q 
      minor[:value] = graph[q][:key]
    end 
  end
  return minor[:id]
end

def prim graph, r
  graph[r][:key] = 0
  queue = []
  graph.each do |k, v|
    queue << k  
  end

  while ! queue.empty?
    u = returns_min queue, graph
    queue.delete u

    graph[u][:neighbors].each do |v|
      v_key = v.keys[0]
      if queue.include?(v_key) && (w(graph, u, v_key) < graph[v_key][:key])
        graph[v_key][:parents] = u
        graph[v_key][:key] = w(graph, u, v_key)
      end
    end
  end
end

prim graph, :a
graph.each do |key, value|
  pp "I'm #{key} and my parent is #{value[:parents]}"
end