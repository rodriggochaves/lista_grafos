require 'pp'

negative_graph = {
  h: {
    neighbors: [{i: 2}]
  },
  i: {
    neighbors: [{j: 3}]
  },
  j: {
    neighbors: [{h: -8}]
  },
}

edges2 = []
vertexes2 = []

negative_graph.each do |k, v|
  vertexes2 << k
  v[:neighbors].each do |p|
    edges2 << [k, p.keys[0], p.values[0]]
  end
end

graph = {
  s: {
    neighbors: [{t: 6}, {y: 7}]
  },
  t: {
    neighbors: [{x: 5}, {y: 8}, {z: -4}]
  },
  x: {
    neighbors: [{t: -2}]
  },
  y: {
    neighbors: [{x: -3}, {z: 9}]
  },
  z: {
    neighbors: [{x: 7}, {s: 2}]
  }
}

edges = []
vertexes = []

graph.each do |k, v|
  vertexes << k
  v[:neighbors].each do |p|
    edges << [k, p.keys[0], p.values[0]]
  end
end

def bellman vertexes, edges, s
  struct = {}

  vertexes.each do |k|
    struct[k] = {
      distance: Float::INFINITY,
      parent: nil
    }
  end
  struct[s][:distance] = 0

  for i in 1..(vertexes.size - 1)
    edges.each do |edge|
      u = edge[0]
      v = edge[1]
      w = edge[2]
      if struct[u][:distance] + w < struct[v][:distance]
        struct[v][:distance] = struct[u][:distance] + w
        struct[v][:parent] = u
      end
    end
  end

  edges.each do |edge|
    u = edge[0]
    v = edge[1]
    w = edge[2]
    if struct[u][:distance] + w < struct[v][:distance]
      puts "Grafo contém um ciclo negativo"
      return false
    end
  end

  return struct
end

# struct = bellman vertexes, edges, :s

# struct = bellman vertexes2, edges2, :h

pp struct